/*
* CPSC 3620 Spring 2020
* Copyright Everett Blakley 2020
* Everett Blakley <everett.blakley@uleth.ca>
*/

#include "BinarySearchTree.h"
#include <iomanip>

using namespace std;

/**
* Prints all the elements of a BST larger than min
*/
void PrintElements(BinarySearchTree::BinaryNode *node, int min)
{
  if (node == nullptr)
  {
    return;
  }
  else if (min >= node->element)
  {
    // All values in the left subtree will be smaller than min
    // We traverse the right subtree
    PrintElements(node->right, min);
  }
  else // (min < node->element)
  {
    // We traverse both subtrees of the node
    cout << node->element << " ";
    PrintElements(node->left, min);
    PrintElements(node->right, min);
  }
}

int main()
{
  string input;
  BinarySearchTree tree;

  cout << "Enter in integers to load into the binary search tree (separated by whitespace, enter non-integer to finish input): ";
  while (cin >> input)
  {
    try
    {
      int x = stoi(input);
      tree.insert(x);
    }
    catch (const exception &)
    {
      cout << "Non-integer value, '" << input << "' found, finishing off tree" << endl;
      break;
    }
  }
  bool quit = tree.root == nullptr;

  if (quit)
  {
    cout << "Tree tree is empty. Terminating" << endl;
  }
  else
  {
    cout << "The elements of the binary search tree are: ";
    tree.printTree();
    cout << endl
         << endl;
  }

  while (!quit)
  {
    cout << "Enter a value to find the elements larger within the tree (enter non-integer to quit): ";
    cin >> input;
    try
    {
      int min = stoi(input);
      cout << "The elements larger than " << min << " are: ";
      PrintElements(tree.root, min);
      cout << endl
           << endl;
    }
    catch (const exception &)
    {
      cout << "Exiting.." << endl;
      quit = true;
      break;
    }
  }

  return 0;
}