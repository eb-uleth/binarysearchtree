# PrintElements for BinarySearchTree

**Description**:  Print all the elements larger than a given input

**Author**: Everett Blakley

**Date**: February 21th, 2020

## Get the code

`git clone https://everettblakley@bitbucket.org/eb-uleth/binarysearchtree.git`

## Compilation

Run `make` in the terminal to compile and run the code. This will produce an executable called `bst`

To clean the directory, run `make clean`

## Program Usage

- When prompted, enter in integers to load in the Binary Search Tree (BST). Integers can be separated by any whitespace character ('\n', '\t', etc).
- To finish the BST, enter in a non-integer (i.e. a string or a character)
- The BST will be printed (in pre-order traversal)

### Example input

```
Enter in integers to load into the binary search tree (separated by whitespace, enter non-integer to finish input): 1 5 9 45 32 65 7 5 f
Non-integer value, 'f' found, finishing off tree
The elements of the binary search tree are: 1 5 9 7 45 32 65
```

```
Enter in integers to load into the binary search tree (separated by whitespace, enter non-integer to finish input): 4
6
3
5
7
2334
453
g
Non-integer value, 'g' found, finishing off tree
The elements of the binary search tree are: 4 3 6 5 7 2334 453
```

*Note: duplicate values will be ignored*

- You will then be prompted to input an integer, and the program will print out all the elements strictly larger than that value. If no value is larger than the minimum, nothing will be printed.

- To terminate, enter in a non-integer (i.e. a string or a character)

### Example of `PrintElement` method

```
The elements of the binary search tree are: 1 5 9 7 45 32 65


Enter a value to find the elements larger within the tree (enter non-integer to quit): 10
The elements larger than 10 are: 45 32 65

Enter a value to find the elements larger within the tree (enter non-integer to quit): 78
The elements larger than 78 are:

Enter a value to find the elements larger within the tree (enter non-integer to quit): 0
The elements larger than 0 are: 1 5 9 7 45 32 65

Enter a value to find the elements larger within the tree (enter non-integer to quit): f
Exiting..
```

## Details

The `BinarySearchTree` abstract data type consists of nodes, where each node has a value (element), and a left node and a right node. Thus it is a recursive data structure. Each node satisfies the property that the left node is smaller than the value and the right node is larger than the value.

The `PrintElements` method takes two arguments:

- `node`: A node of a BST
- `min`: the value you want to print the elements larger than

The actual implementation of `PrintElements` can be found in `main.cpp`, but the pseudo-code execution is as follows:

- If the `node` is null, exit
- Else, if the value of `min` is greater than or equal to `node`, traverse the right subtree
  - By the properties of BST's, we know the values of left subtree will all be smaller, hence we look for values larger than the current node (i.e. to the right)
- Else, the value of `min` is less than `node`, print the value of the current node and then traverse both subtrees