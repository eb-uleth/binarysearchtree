/*
* CPSC 3620 Spring 2020
* Implementation from: Weiss Textbook
* Everett Blakley <everett.blakley@uleth.ca>
*/

#ifndef BINARY_SEARCH_TREE_H
#define BINARY_SEARCH_TREE_H

#include <iostream>

class BinarySearchTree
{
public:
  BinarySearchTree();
  ~BinarySearchTree();
  BinarySearchTree(const BinarySearchTree &rhs);
  BinarySearchTree(BinarySearchTree &&rhs);
  const int &findMin() const;
  const int &findMax() const;
  bool contains(const int &x) const;
  bool isEmpty() const;
  void printTree(std::ostream &out = std::cout) const;
  void makeEmpty();
  void insert(const int &x);
  void insert(int &&x);
  void remove(const int &x);
  BinarySearchTree &operator=(const BinarySearchTree &rhs);
  BinarySearchTree &operator=(BinarySearchTree &&rhs);
  struct BinaryNode
  {
    int element;
    BinaryNode *left;
    BinaryNode *right;
    BinaryNode(const int &theElement, BinaryNode *lt, BinaryNode *rt)
        : element{theElement}, left{lt}, right{rt} {}
    BinaryNode(int &&theElement, BinaryNode *lt, BinaryNode *rt)
        : element{std::move(theElement)}, left{lt}, right{rt} {}
  };
  BinaryNode *root;

private:
  void insert(const int &x, BinaryNode *&t);
  void insert(int &&x, BinaryNode *&t);
  void remove(const int &x, BinaryNode *&t);
  BinaryNode *findMin(BinaryNode *t) const;
  BinaryNode *findMax(BinaryNode *t) const;
  bool contains(const int &x, BinaryNode *t) const;
  void makeEmpty(BinaryNode *&t);
  void printTree(BinaryNode *t, std::ostream &out) const;
  BinaryNode *clone(BinaryNode *t) const;
};

#endif // BINARY_SEARCH_TREE_H