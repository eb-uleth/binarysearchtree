/*
* CPSC 3620 Spring 2020
* Implementation from: Weiss Textbook
* Everett Blakley <everett.blakley@uleth.ca>
*/
#include "BinarySearchTree.h"

using namespace std;

/****************
 * PUBLIC METHODS BELOW
*****************/

/**
 * Default constructor
 */
BinarySearchTree::BinarySearchTree() : root(nullptr) {}

/**
* Destructor for the tree
*/
BinarySearchTree::~BinarySearchTree()
{
  makeEmpty();
}

/**
* Copy constructor
*/
BinarySearchTree::BinarySearchTree(const BinarySearchTree &rhs) : root{nullptr}
{
  root = clone(rhs.root);
}

/**
* Returns the value of the minimum node of the tree
*/
const int &BinarySearchTree::findMin() const
{
  return findMin(root)->element;
}

/**
* Returns the value of the maximum node of the tree
*/
const int &BinarySearchTree::findMax() const
{
  return findMax(root)->element;
}

/**
* Returns true if x is found in the tree.
*/
bool BinarySearchTree::contains(
    const int &x) const
{
  return contains(x, root);
}

/*
* Returns true if the tree is empty
*/
bool BinarySearchTree::isEmpty() const
{
  return false;
}

/*
* Printst the tree to the console in some order
*/
void BinarySearchTree::printTree(std::ostream &out) const
{
  if (root != nullptr)
  {
    printTree(root, out);
  }
  out << endl;
}

/*
* Empties the tree
*/
void BinarySearchTree::makeEmpty()
{
  makeEmpty(root);
}

/**
* Insert x into the tree; duplicates are ignored.
*/
void BinarySearchTree::insert(const int &x)
{
  insert(x, root);
}

/**
* Remove x from the tree. Nothing is done if x is not found.
*/
void BinarySearchTree::remove(const int &x)
{
  remove(x, root);
}

// /*
// * Overloaded constant assignment operator
// */
// BinarySearchTree &BinarySearchTree::operator=(
//     const BinarySearchTree &rhs)
// {
//   /* UNIMPLEMENTED */
// }

// /*
// * Overloaded assignment operator
// */
// BinarySearchTree &BinarySearchTree::operator=(
//     BinarySearchTree &&rhs)
// {
//   /* UNIMPLEMENTED */
// }

/****************
 * PRIVATE METHODS BELOW
*****************/

/**
* Internal method to insert into a subtree.
* x is the item to insert.
* t is the node that roots the subtree.
* Set the new root of the subtree.
*/
void BinarySearchTree::insert(
    const int &x,
    BinarySearchTree::BinaryNode *&t)
{
  if (t == nullptr)
    t = new BinaryNode{x, nullptr, nullptr};
  else if (x < t->element)
    insert(x, t->left);
  else if (t->element < x)
    insert(x, t->right);
  else
  {
  } // Duplicate; do nothing
}

/**
* Internal method to insert into a subtree.
* x is the item to insert by moving.
* t is the node that roots the subtree.
* Set the new root of the subtree.
*/
void BinarySearchTree::insert(
    int &&x,
    BinarySearchTree::BinaryNode *&t)
{
  if (t == nullptr)
    t = new BinaryNode{std::move(x), nullptr, nullptr};
  else if (x < t->element)
    insert(std::move(x), t->left);
  else if (t->element < x)
    insert(std::move(x), t->right);
  else
  {
  } // Duplicate; do nothing
}

/**
* Internal method to remove from a subtree.
* x is the item to remove.
* t is the node that roots the subtree.
* Set the new root of the subtree.
*/
void BinarySearchTree::remove(
    const int &x,
    BinarySearchTree::BinaryNode *&t)
{
  if (t == nullptr)
    return;
  // Item not found; do nothing
  if (x < t->element)
    remove(x, t->left);
  else if (t->element < x)
    remove(x, t->right);
  else if (t->left != nullptr && t->right != nullptr) // Two children
  {
    t->element = findMin(t->right)->element;
    remove(t->element, t->right);
  }
  else
  {
    BinaryNode *oldNode = t;
    t = (t->left != nullptr) ? t->left : t->right;
    delete oldNode;
  }
}

/**
* Internal method to find the smallest item in a subtree t.
* Return node containing the smallest item.
*/
typename BinarySearchTree::BinaryNode *BinarySearchTree::findMin(
    BinarySearchTree::BinaryNode *t) const
{
  if (t == nullptr)
    return nullptr;
  if (t->left == nullptr)
    return t;
  return findMin(t->left);
}

/**
* Internal method to find the largest item in a subtree t.
* Return node containing the largest item.
*/
typename BinarySearchTree::BinaryNode *BinarySearchTree::findMax(
    BinarySearchTree::BinaryNode *t) const
{
  if (t != nullptr)
    while (t->right != nullptr)
      t = t->right;
  return t;
}

/**
* Internal method to test if an item is in a subtree.
* x is item to search for.
* t is the node that roots the subtree.
*/
bool BinarySearchTree::contains(
    const int &x,
    BinarySearchTree::BinaryNode *t) const
{
  if (t == nullptr)
    return false;
  else if (x < t->element)
    return contains(x, t->left);
  else if (t->element < x)
    return contains(x, t->right);
  else
    return true;
  // Match
}

/**
* Internal method to make subtree empty.
*/
void BinarySearchTree::makeEmpty(
    BinarySearchTree::BinaryNode *&t)
{
  if (t != nullptr)
  {
    makeEmpty(t->left);
    makeEmpty(t->right);
    delete t;
  }
  t = nullptr;
}

void BinarySearchTree::printTree(
    BinarySearchTree::BinaryNode *t,
    std::ostream &out) const
{
  if (t != nullptr)
  {
    out << t->element << " ";
    printTree(t->left, out);
    printTree(t->right, out);
  }
}

/**
* Internal method to clone subtree.
*/
typename BinarySearchTree::BinaryNode *BinarySearchTree::clone(
    BinarySearchTree::BinaryNode *t) const
{
  if (t == nullptr)
    return nullptr;
  else
    return new BinaryNode{t->element, clone(t->left), clone(t->right)};
}