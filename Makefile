CXX=g++
CXX_LANG_FLAGS=-std=c++0x -g
CXX_WARN_FLAGS=-Wall

SRC_DIR = ./

PROGRAM_DIR = .

SRC_INCLUDE = ./
INCLUDE = -I ${SRC_INCLUDE}

CXXFLAGS= $(CXX_LANG_FLAGS) $(CXX_WARN_FLAGS) $(INCLUDE)

PROGRAM = ./bst

all: $(PROGRAM)
	$(PROGRAM)

%.o: %.cpp
	$(CXX) $(CXX_LANG_FLAGS) -c $< -o $@

$(PROGRAM): $(PROGRAM_DIR)
	$(CXX) $(CXXFLAGS) -o $(PROGRAM) \
	$(PROGRAM_DIR)/*.cpp

.PHONY: clean
clean:
	rm -rf *~ ./*.o $(PROGRAM)